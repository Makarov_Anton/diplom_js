'use strict';

class Vector {
    constructor(x = 0, y = 0) {
        this.x = x;
        this.y = y;
    }

    plus(vector) {
        if (!(vector instanceof Vector)) {
            throw new Error('В метод plus передан не объект класса Vector');
        }
        return new Vector((this.x + vector.x), this.y + vector.y);
    }

    times(factor) {
        return new Vector((this.x * factor), this.y * factor);
    }
}

class Actor {
    constructor(position = new Vector(0, 0), size = new Vector(1, 1), speed = new Vector(0, 0)) {
        if (!(position instanceof Vector) || !(size instanceof Vector) || !(speed instanceof Vector)) {
            throw new Error('В конструктор передан не объект класса Vector');
        }
        this.pos = position;
        this.size = size;
        this.speed = speed;
    }

    act() {

    }

    get type() {
        return 'actor'
    }

    get left() {
        return this.pos.x;
    }

    get right() {
        return this.pos.x + this.size.x;
    }

    get top() {
        return this.pos.y;
    }

    get bottom() {
        return this.pos.y + this.size.y;
    }

    isIntersect(otherActor) {
        if (!(otherActor instanceof Actor)) {
            throw new Error('В качестве аргумента передан не объект класса Actor');
        }
        if (this === otherActor) {
            return false;
        }
        return this.right > otherActor.left &&
            this.left < otherActor.right &&
            this.top < otherActor.bottom &&
            this.bottom > otherActor.top;
    }
}


class Level {
    constructor(grid = [], actorsArray = []) {
        this.grid = grid.slice();
        this.actors = actorsArray.slice();
        this.player = this.actors.find(actor => actor.type === 'player');
        this.height = grid.length;
        this.width = grid.reduce((max, elem) => (elem.length > max) ? elem.length : max, 0);
        this.status = null;
        this.finishDelay = 1;
    }

    isFinished() {
        return this.status !== null && this.finishDelay < 0;
    }

    actorAt(actor) {
        if (!(actor instanceof Actor)) {
            throw new Error('В качестве аргумента передан не объект класса Actor');
        }
        return this.actors.find(actorElem => actorElem.isIntersect(actor));
    }

    obstacleAt(newPosition, actorSize) {
        if (!(newPosition instanceof Vector) || !(actorSize instanceof Vector)) {
            throw new Error('В качестве аргумента передан не объект класса Vector');
        }

        const leftBorder = Math.floor(newPosition.x);
        const rightBorder = Math.ceil(newPosition.x + actorSize.x);
        const topBorder = Math.floor(newPosition.y);
        const bottomBorder = Math.ceil(newPosition.y + actorSize.y);

        if (bottomBorder > this.height) {
            return 'lava';
        }
        if (leftBorder < 0 || rightBorder > this.width || topBorder < 0) {
            return 'wall';
        }

        for (let y = topBorder; y < bottomBorder; y++) {
            for (let x = leftBorder; x < rightBorder; x++) {
                const obstacle = this.grid[y][x];
                if (obstacle) {
                    return obstacle;
                }
            }
        }
    }

    removeActor(actor) {
        const indexActor = this.actors.indexOf(actor);
        if (indexActor !== -1) {
            this.actors.splice(indexActor, 1);
        }
    }

    noMoreActors(type) {
        return !this.actors.some(actor => actor.type === type);
    }

    playerTouched(actorType, actor = {}) {
        if (this.status !== null) {
            return;
        }
        if (actorType === 'lava' || actorType === 'fireball') {
            this.status = 'lost';
        }
        if (actorType === 'coin' && actor.type === 'coin') {
            this.removeActor(actor);
            if (this.noMoreActors(actorType)) {
                this.status = 'won';
            }
        }
    }
}

class LevelParser {
    constructor(dictionary = {}) {
        this.dictionary = Object.assign({}, dictionary);
    }

    actorFromSymbol(symbol) {
       return this.dictionary[symbol];
    }

    obstacleFromSymbol(symbol) {
        switch (symbol) {
            case 'x':
                return 'wall';
            case '!':
                return 'lava';
        }
    }

    createGrid(plan) {
        return plan.map(line => line.split('')).map(line => line.map(cell => this.obstacleFromSymbol(cell)));
    }

    createActors(plan) {
        return plan.reduce((result, line, y) => {
            line.split('').forEach((cell, x) => {
                const constructor = this.actorFromSymbol(cell);
                if (typeof constructor === 'function') {
                    const actor = new constructor(new Vector(x, y));
                    if (actor instanceof Actor) {
                        result.push(actor);
                    }
                }
            });
            return result;
        }, [])
    }

    parse(plan) {
        return new Level(this.createGrid(plan), this.createActors(plan));
    }
}

class Fireball extends Actor {
    constructor(position = new Vector(0, 0), speed = new Vector(0, 0)) {
        super(position, new Vector(1, 1), speed);
    }

    get type() {
        return 'fireball'
    }

    getNextPosition(time = 1) {
        return this.pos.plus(this.speed.times(time));
    }

    handleObstacle() {
        this.speed = this.speed.times(-1);
    }

    act(time, level) {
        if (level.obstacleAt(this.getNextPosition(time), this.size)) {
            this.handleObstacle();
        } else {
            this.pos = this.getNextPosition(time);
        }
    }
}

class HorizontalFireball extends Fireball {
    constructor(position = new Vector(0, 0)) {
        super(position, new Vector(2, 0));
    }
}

class VerticalFireball extends Fireball {
    constructor(position = new Vector(0, 0)) {
        super(position, new Vector(0, 2));
    }
}

class FireRain extends Fireball {
    constructor(position = new Vector(0, 0)) {
        super(position, new Vector(0, 3));
        this.startPos = position;
    }

    handleObstacle() {
        this.pos = this.startPos;
    }
}

class Coin extends Actor {
    constructor(position = new Vector(0, 0)) {
        super(position.plus(new Vector(0.2, 0.1)), new Vector(0.6, 0.6));
        this.springSpeed = 8;
        this.springDist = 0.07;
        this.spring = Math.random() * Math.PI * 2;
        this.startPos = this.pos;
    }

    get type() {
        return 'coin'
    }

    updateSpring(time = 1) {
        this.spring += this.springSpeed * time;
    }

    getSpringVector() {
        return new Vector(0, Math.sin(this.spring) * this.springDist)
    }

    getNextPosition(time = 1) {
        this.updateSpring(time);
        return this.startPos.plus(this.getSpringVector());
    }
    act(time) {
        this.pos = this.getNextPosition(time);
    }
}

class Player extends Actor {
    constructor(position = new Vector(0, 0)) {
        super(position.plus(new Vector(0, -0.5)), new Vector(0.8, 1.5), new Vector(0, 0));
    }

    get type() {
        return 'player'
    }
}

const dictionary = {
    '@': Player,
    'v': FireRain,
    'o': Coin,
    '=': HorizontalFireball,
    '|': VerticalFireball
};

const parser = new LevelParser(dictionary);

loadLevels()
    .then((res) => {
        runGame(JSON.parse(res), parser, DOMDisplay)
            .then(() => alert('Вы выиграли!'))
    });

