@extends('index')
@section('title', 'Главная страница')
@section('content')
    <div class="container">

        <div class="row">
            @foreach($themes as $theme)

                <div class="col-xs-12 col-md-12">
                    <div>
                        <a class="btn btn-info btn-lg">{{ $theme->title }} </a>
                    </div>
                </div>
                @foreach($theme->faqs as $faq)
                    <div class="col-xs-12 col-md-4">
                        <div>
                            <a style="color: green" class="btn btn-info btn-lg">{{ $faq->title }}</a>
                        </div>
                        <p style="color: red">{!! $faq->answer !!}</p>

                    </div>
                @endforeach
            @endforeach

        </div>
    </div>

@endsection