<?php
trait mass{
  public function mass(){
    if ($this->mass > 10){
      $this->discount;
      return  $this->discount;
    }else{
      $this->discount;
      return $this->discount = 0;
    }
  }
}

interface methods
{
public function getDiscount();
public function getDelivery();
}

abstract class Product
{
  public $price;
  public $category;
  public $discount;
  public $name;
  public $delivery;

  public function __construct($name, $category, $price, $discount = 0)
  {
    $this->name = $name;
    $this->category = $category;
    $this->price = $price;
    $this->discount = $discount;
  }

  public function getDelivery()
  {
    if ($this->discount){
      return $this->delivery = 300;
    }else{
      return $this->delivery = 250;
    }
  }

  public function getDiscount()
  {
    if ($this->discount){
      return round($this->price - ($this->price / $this->discount), 2);
    }else{
      return $this->price;
    }
  }

}

class Vegetables extends Product implements methods
{
  public $mass;
  use mass;
}

$potato = new Vegetables("Potato", "Vegetables.class", 150, 10);
$potato->mass = 15;
$potato->mass();
echo "<br>Наименование: {$potato->name}<br>Масса продука: {$potato->mass}кг.<br>Цена товара: {$potato->price}p.<br>" .
    "Цена товара со скидкой: {$potato->getDiscount()}p.<br>Доставка: {$potato->getDelivery()}p.<br>";

$tomato = new Vegetables("Tomato", "Vegetables.class", 150, 10);
$tomato->mass = 9;
$tomato->mass();
echo "<br>Наименование: {$tomato->name}<br>Масса продука: {$tomato->mass}кг.<br> Цена товара: {$tomato->price}p.<br>" .
    "Цена товара со скидкой: {$tomato->getDiscount()}p.<br>Доставка: {$tomato->getDelivery()}p.<br>";

class Fruits extends Product implements methods
{

}

$apples = new Fruits("Apples", "Fruits.class", 210, 10);
echo "<br>Наименование: {$apples->name}<br> Цена товара: {$apples->price}p.<br>" .
  "Цена товара со скидкой: {$apples->getDiscount()}p.<br>Доставка: {$apples->getDelivery()}p.<br>";

$banana = new Vegetables("Banana", "Fruits.class", 210, 10);
echo "<br>Наименование: {$banana->name}<br> Цена товара: {$banana->price}p.<br>" .
  "Цена товара со скидкой: {$banana->getDiscount()}p.<br>Доставка: {$banana->getDelivery()}p.<br>";

class Juice extends Product implements methods
{

}

$applesJuice = new Fruits("Apple juice", "Juice.class", 110, 10);
echo "<br>Наименование: {$applesJuice->name}<br> Цена товара: {$applesJuice->price}p.<br>" .
  "Цена товара со скидкой: {$applesJuice->getDiscount()}p.<br>Доставка: {$applesJuice->getDelivery()}p.<br>";

$bananaJuice = new Vegetables("Banana juice", "Juice.class", 110, 10);
echo "<br>Наименование: {$bananaJuice->name}<br> Цена товара: {$bananaJuice->price}p.<br>" .
  "Цена товара со скидкой: {$bananaJuice->getDiscount()}p.<br>Доставка: {$bananaJuice->getDelivery()}p.<br>";
