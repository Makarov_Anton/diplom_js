<?php
/**
 * Created by PhpStorm.
 * User: Sec
 * Date: 03.03.2018
 * Time: 11:29
 */

namespace classes;

class Order {

  public function quantity($bag) {
    $quantity = 0;
    foreach($bag->items as $bagItem) $quantity += $bagItem->quantity;
    echo "<br>Итого количиство едениц товаров: " . $quantity . 'шт.<br>';
  }
  public function total($bag) {
    $total = 0;
    foreach($bag->items as $bagItem) $total += $bagItem->subtotal;
    echo "Стоимость всех товаров в корзине: " . $total . 'руб.<br>';
  }

  public function print($bag) {
    foreach ($bag->items as $bags){
      echo "ТОВАРНЫЙ ЧЕК<br>";
      echo "<br>Товар: {$bags->name}" .
        "<br>Цена: {$bags->price}руб." .
        "<br>Количество: {$bags->quantity}шт." .
        "<br>Скидка: {$bags->discount}%<br>";
    }
  }

}

