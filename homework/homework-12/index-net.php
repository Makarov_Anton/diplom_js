<?php
//phpinfo();
$isbn = "";
$name = "";
$author = "";
define('DB_DRIVER','mysql');
define('DB_HOST','localhost');
define('DB_NAME','amakarov');
define('DB_USER','amakarov');
define('DB_PASS','neto1581');
try
{

  $connect_str = DB_DRIVER . ':host=' . DB_HOST . ';dbname=' . DB_NAME;
  $db = new PDO($connect_str, DB_USER, DB_PASS);

  $post = $_POST;
  if (isset($post['isbn']) || isset($post['name']) || isset($post['author'])){
    $isbn = $post['isbn'];
    $name = $post['name'];
    $author = $post['author'];
    $sql = ("select * from books where isbn LIKE '%{$post['isbn']}%' and name LIKE '%{$post['name']}%' and author LIKE '%{$post['author']}%'");
  }else{
    $sql = ("select * from books");
  }
  if ($result = $db->query($sql)) {
    foreach ($result as $row) {
      $data[] = array(
        "name" => $row['name'],
        "author" => $row['author'],
        "year" => $row['year'],
        "isbn" => $row['isbn'],
        "genre" => $row['genre']
      );
    }
  }

}

catch(PDOException $e)
{
  die("Error: ".$e->getMessage());
}
?>

<!doctype html>
<html lang="ru">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
  <title></title>
  <style>
    table {
      border-spacing: 0;
      border-collapse: collapse;
    }

    table td, table th {
      border: 1px solid #ccc;
      padding: 5px;
    }

    table th {
      background: #eee;
    }
  </style>
</head>
<body>

<h1>Библиотека успешного человека</h1>

<form method="POST" action="index.php">
  <input type="text" name="isbn" placeholder="ISBN" value="<?= $isbn ?>" />
  <input type="text" name="name" placeholder="Название книги" value="<?= $name ?>" />
  <input type="text" name="author" placeholder="Автор книги" value="<?= $author ?>" />
  <input type="submit" value="Поиск" />
</form>

<table>
  <tr>
    <th>Название</th>
    <th>Автор</th>
    <th>Год выпуска</th>
    <th>Жанр</th>
    <th>ISBN</th>
  </tr>

  <?php
  if (empty($data)){
    echo "По вашему запросу не чего не найдено.";
  }else{
    foreach ($data as $date){?>
      <tr>
        <td><?= $date['name']?></td>
        <td><?= $date['author']?></td>
        <td><?= $date['year']?></td>
        <td><?= $date['genre']?></td>
        <td><?= $date['isbn']?></td>
      </tr>
    <?php }} ?>
</table>
</body>
</html>
