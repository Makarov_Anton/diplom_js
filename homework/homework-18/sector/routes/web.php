<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'SectorController@getData');

Route::get('/create', 'SectorController@create');

Route::get('/delete', 'SectorController@delete');

Route::get('/update', 'SectorController@update');

Route::get('/update/line', 'SectorController@updateLine');
