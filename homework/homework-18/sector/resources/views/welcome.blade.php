@extends('layouts.app')
@section('container')

    <table class="item">
        <tr>
            <th>№п/п</th>
            <th>Фамилия</th>
            <th>Имя</th>
            <th>Отчество</th>
            <th>Телефон</th>
            <th></th>
        </tr>
        @foreach ($data as $item)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td>{{ $item->surname }}</td>
                <td>{{ $item->name }}</td>
                <td>{{ $item->patronymic }}</td>
                <td>{{ $item->phone }}</td>
                <td>
                    <form action="/update">
                        <input type="hidden" name="id" value="{{ $item->id }}">
                        <input type="submit" value="Редактировать">
                    </form>
                </td>
            </tr>
        @endforeach
        <hr>
    </table>
    <a href="/create">Добавить контакт в таблицу</a>
@endsection         
       