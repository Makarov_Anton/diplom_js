<?php

/* task.twig */
class __TwigTemplate_b54e89614de540b462677b4b60895438c2f3c6576f48dff0c7507901dfa3f4f7 extends Twig_Template
{
    private $source;

    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<!doctype html>
<html lang=\"ru\">
<head>
    <meta charset=\"UTF-8\">
    <meta name=\"viewport\">
    <title></title>

    <style>
        table {
            border-spacing: 0;
            border-collapse: collapse;
        }

        table td, table th {
            border: 1px solid #ccc;
            padding: 5px;
        }

        table th {
            background: #eee;
        }
    </style>

</head>
<body>

<h1>Добро пожаловать, ";
        // line 28
        echo twig_escape_filter($this->env, ($context["login"] ?? null), "html", null, true);
        echo ". Твой список дел на сегодня.</h1>
<div style=\"float: left\">
    <form method=\"POST\">

        <input type=\"text\" name=\"description\" placeholder=\"Описание задачи\" value=";
        // line 32
        echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
        echo ">
        <input type=\"submit\" name=\"save\" value=\"Добавить\">

    </form>
</div>
<div style=\"float: left; margin-left: 20px;\">
    <form method=\"POST\">
        <label for=\"sort\">Сортировать по:</label>
        <select name=\"sort_by\">
            <option value=\"date_created\">Дате добавления</option>
            <option value=\"is_done\">Статусу</option>
            <option value=\"description\">Описанию</option>
        </select>
        <input type=\"submit\" name=\"sort\" value=\"Отсортировать\"/>
    </form>
</div>
<div style=\"clear: both\"></div>

<table>
    <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
        <th>Ответственный</th>
        <th>Автор</th>
        <th>Закрепить задачу за пользователем</th>
    </tr>

        ";
        // line 61
        if (twig_test_empty(($context["data"] ?? null))) {
            // line 62
            echo "            <h3> Дел пока нет, кайфуй.</h3>
        ";
        } else {
            // line 64
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["data"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["date"]) {
                // line 65
                echo "                ";
                if ((twig_get_attribute($this->env, $this->source, $context["date"], "description", array(), "any", true, true) && (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["date"], "description", array())) > 3))) {
                    // line 66
                    echo "
    <tr>
        <td>";
                    // line 68
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["date"], "description", array()), "html", null, true);
                    echo "</td>
        <td>";
                    // line 69
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["date"], "date_added", array()), "html", null, true);
                    echo "</td>
        <td style=\"color: ";
                    // line 70
                    if ((twig_get_attribute($this->env, $this->source, $context["date"], "is_done", array()) == 0)) {
                        echo " orange ";
                    } else {
                        echo " green ";
                    }
                    echo "\">
            ";
                    // line 71
                    if ((twig_get_attribute($this->env, $this->source, $context["date"], "is_done", array()) == 0)) {
                        echo " В процессе ";
                    } else {
                        echo " Выполнено ";
                    }
                    // line 72
                    echo "        </td>
        <td>
            <a href='?id=";
                    // line 74
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["date"], "id", array()), "html", null, true);
                    echo "&action=edit'>Изменить</a>
            <a href='?id=";
                    // line 75
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["date"], "id", array()), "html", null, true);
                    echo "&action=done'>Выполнить</a>
            <a href='?id=";
                    // line 76
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["date"], "id", array()), "html", null, true);
                    echo "&action=delete'>Удалить</a>
        </td>
        <td>";
                    // line 78
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["date"], "assigned", array()), "html", null, true);
                    echo "</td>
        <td>";
                    // line 79
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["date"], "author", array()), "html", null, true);
                    echo "</td>
        <td>
            <form method=\"POST\">
                <select name=\"assigned_user_id\">
                    ";
                    // line 83
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["users"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                        // line 84
                        echo "                        <option value=\"";
                        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["date"], "id", array()), "html", null, true);
                        echo ";";
                        echo twig_escape_filter($this->env, $context["user"], "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $context["user"], "html", null, true);
                        echo "</option>
                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 86
                    echo "                </select>
                <input type=\"submit\" name=\"assign\" value=\"Переложить ответственность\">
            </form>
        </td>
    </tr>

                ";
                }
                // line 93
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['date'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 94
            echo "        ";
        }
        // line 95
        echo "</table>
<div>
    <h3>Также, посмотрите, что от Вас требуют другие люди:</h3>
</div>

<table>
    <tr>
        <th>Описание задачи</th>
        <th>Дата добавления</th>
        <th>Статус</th>
        <th></th>
        <th>Ответственный</th>
        <th>Автор</th>
    </tr>
    ";
        // line 109
        if (twig_test_empty(($context["assigned_data"] ?? null))) {
            // line 110
            echo "        <h3> Дел пока нет, кайфуй.</h3>
    ";
        } else {
            // line 112
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["assigned_data"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["dates"]) {
                // line 113
                echo "            ";
                if ((twig_get_attribute($this->env, $this->source, $context["dates"], "description", array(), "any", true, true) && (twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dates"], "description", array())) > 3))) {
                    // line 114
                    echo "    <tr>
        <td>";
                    // line 115
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dates"], "description", array()), "html", null, true);
                    echo "</td>
        <td>";
                    // line 116
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dates"], "date_added", array()), "html", null, true);
                    echo "</td>
        <td style=\"color: ";
                    // line 117
                    if ((twig_get_attribute($this->env, $this->source, $context["dates"], "is_done", array()) == 0)) {
                        echo " orange ";
                    } else {
                        echo " green ";
                    }
                    echo "\">
            ";
                    // line 118
                    if ((twig_get_attribute($this->env, $this->source, $context["dates"], "is_done", array()) == 0)) {
                        echo " В процессе ";
                    } else {
                        echo " Выполнено ";
                    }
                    // line 119
                    echo "        </td>
        <td>
            <a href='?id=";
                    // line 121
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dates"], "id", array()), "html", null, true);
                    echo "&action=edit'>Изменить</a>
            <a href='?id=";
                    // line 122
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dates"], "id", array()), "html", null, true);
                    echo "&action=done'>Выполнить</a>
            <a href='?id=";
                    // line 123
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dates"], "id", array()), "html", null, true);
                    echo "&action=delete'>Удалить</a>
        </td>
        <td>";
                    // line 125
                    echo twig_escape_filter($this->env, ($context["login"] ?? null), "html", null, true);
                    echo "</td>
        <td>";
                    // line 126
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["dates"], "author", array()), "html", null, true);
                    echo "</td>
    </tr>
            ";
                }
                // line 129
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dates'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 130
            echo "    ";
        }
        // line 131
        echo "
</table>

<div style=\"font-weight: bold; font-size: 14px; color: #1e7e34; margin: 50px 0;\">
    <a href=\"../controller/controller_task.php?logout=1\">Выйти</a>
</div>
</body>
</html>";
    }

    public function getTemplateName()
    {
        return "task.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  285 => 131,  282 => 130,  276 => 129,  270 => 126,  266 => 125,  261 => 123,  257 => 122,  253 => 121,  249 => 119,  243 => 118,  235 => 117,  231 => 116,  227 => 115,  224 => 114,  221 => 113,  216 => 112,  212 => 110,  210 => 109,  194 => 95,  191 => 94,  185 => 93,  176 => 86,  163 => 84,  159 => 83,  152 => 79,  148 => 78,  143 => 76,  139 => 75,  135 => 74,  131 => 72,  125 => 71,  117 => 70,  113 => 69,  109 => 68,  105 => 66,  102 => 65,  97 => 64,  93 => 62,  91 => 61,  59 => 32,  52 => 28,  23 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "task.twig", "D:\\netology\\homework\\homework-17\\templates\\task.twig");
    }
}
