<?php

function dbConnect()
{
  define('DB_DRIVER', 'mysql');
  define('DB_HOST', '127.0.0.1');
  define('DB_NAME', 'books');
  define('DB_USER', 'mysql');
  define('DB_PASS', 'mysql');
  $connect_str = DB_DRIVER . ':host=' . DB_HOST . ';dbname=' . DB_NAME;
  $db = new PDO($connect_str, DB_USER, DB_PASS);
  return $db;
}


function addslash($array)
{
  $arr = array();
  foreach ($array as $key => $value) {
    $arr[$key] = addslashes($value);
  }
  return $arr;
}

function sort_print_task($post, $temp_sql)
{

    switch ($post['sort_by']) {
      case "date_created":
        $sql = $temp_sql . " ORDER BY date_added";
        break;
      case "is_done":
        $sql = $temp_sql . " ORDER BY is_done";
        break;
      case "description":
        $sql = $temp_sql . " ORDER BY description";
        break;
    }
    return $sql;

}

function print_task($sql, $db)
{
  $data = array();
  if ($result = $db->query($sql)) {
    foreach ($result as $row) {
      $data[] = array(
        "id" => $row['id'],
        "user_id" => $row['user_id'],
        "date_added" => $row['date_added'],
        "description" => $row['description'],
        "is_done" => $row['is_done'],
        "author" => $row['author'],
        "assigned" => $row['assigned'],
      );
    }

    return $data;
  }
}

